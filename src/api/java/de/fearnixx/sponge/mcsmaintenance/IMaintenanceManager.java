package de.fearnixx.sponge.mcsmaintenance;

import de.fearnixx.sponge.mcsmaintenance.set.IMaintenanceSet;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.entity.living.player.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Life4YourGames on 20.03.17.
 */
public interface IMaintenanceManager {

    boolean isEnabled();

    boolean registerSet(IMaintenanceSet set);
    void removeSet(IMaintenanceSet set);
    Optional<IMaintenanceSet> getSet(String name);
    Map<String, IMaintenanceSet> getAvailableSets();
    List<IMaintenanceSet> getActiveSets();
    boolean enableSet(String name);
    boolean enableSet(IMaintenanceSet set);
    boolean disableSet(String name);
    boolean disableSet(IMaintenanceSet set);

    boolean playerRequestExemption(String name);
    boolean playerRequestExemption(User user);
    int playerAddExemption(String name, int count);
    boolean playerSetExemption(String name, int count);

    ConfigurationNode getConfig();
}
