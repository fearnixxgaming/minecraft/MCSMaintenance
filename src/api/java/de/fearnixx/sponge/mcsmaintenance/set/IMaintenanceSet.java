package de.fearnixx.sponge.mcsmaintenance.set;

import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

import java.util.Optional;

/**
 * Created by Life4YourGames on 26.11.16.
 */
public interface IMaintenanceSet {

    /**
     * Name of this set.
     * This has to be unique
     * @return name of the set
     */
    String getName();

    /**
     * Whether or not to enforce the rules on the current players.
     * @return boolean
     */
    boolean enforceOnEnable();

    /**
     * Whether or not this set is even supposed to return false at any circumstance
     * @see #checkPlayerAgainstServer(User)
     * @return boolean
     */
    boolean restrictsJoin();

    /**
     * Called to issue a check whether or not this set allows <code>user</code> to join/stay on the server
     * @param user the user to check
     * @return Whether or not the user should not be rejected/kicked
     */
    boolean checkPlayerAgainstServer(User user);

    /**
     * Called to issue a check whether or not this set allows <code>user</code> to be in/join world <code>world</code>
     * @param user the user to check
     * @param to the world the user tries to enter
     * @param from **MAY BE NULL** the world the user comes from
     * @return <code>Optional.empty()</code> for no denial.
     *         <code>Transform\<World\></code> for redirection
     *         <code>Optional.of(from)</code> just cancels the event
     */
    Optional<Transform<World>> checkPlayerAgainstWorld(User user, Transform<World> to, Transform<World> from);

    /**
     * Whether or not this set requires users to have a special permission in order to enable it
     * Remember: If the user doesn't have this exact permission,
     * the plugin will append ".start" and ".stop" to this string in order to generate different
     * permissions for toggling this set on and off
     * @return <code>Optional.empty()</code> for default permission
     */
    Optional<String> getTogglePerm();

    /**
     * Self-explaining
     * @return <code>Optional.empty()</code> for no change to the MOTD
     */
    Optional<Text> getMOTD();

    /**
     * Priority used in methods like getMOTD to decide which should be displayed
     * Higher overwrites
     */
    int getPriority();

    /**
     * Manager tells the set to reload itself
     */
    void reload();

    /**
     * Manager asks to send information about this set to <code>CommandSource</code>
     */
    Text getInfo();

    String getInfoCommand();

    String getEditCommand();
}
