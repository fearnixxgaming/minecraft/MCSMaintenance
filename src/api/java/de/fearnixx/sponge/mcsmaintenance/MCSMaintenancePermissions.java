package de.fearnixx.sponge.mcsmaintenance;

/**
 * Created by Life4YourGames on 01.04.17.
 */
public abstract class MCSMaintenancePermissions {

    public static final String MAINT_COMMAND = "maint.command";
    public static final String MAINT_EXEMPT= "maint.exempt.command";
    public static final String MAINT_EXEMPT_JOIN= "maint.exempt.join";
    public static final String MAINT_EXEMPT_WORLDS= "maint.exempt.worlds";
    public static final String MAINT_EXEMPT_WORLD= "maint.exempt.world";

    public static final String MAINT_COMM_EXEMPT = "maint.command.exempt.self";
    public static final String MAINT_COMM_EXEMPT_OTHER = "maint.command.exempt.others";

    public static final String MAINT_SET_EDIT = "maint.set.edit";
    public static final String MAINT_SET_DEL = "maint.set.delete";
    public static final String MAINT_SET_CREATE = "maint.set.create";
    public static final String MAINT_SET_TOGGLE = "maint.set.toggle";
    public static final String MAINT_SET_SHOW = "maint.set.show";
    public static final String MAINT_SET_LIST = "maint.set.list";
}
