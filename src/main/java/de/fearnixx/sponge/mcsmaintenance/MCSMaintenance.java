package de.fearnixx.sponge.mcsmaintenance;

import com.google.inject.Inject;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStoppingEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Main plugin class
 */
@Plugin(id = "mcsmaintenance", version = "0.0.9", name = "FNG_MCSMaintenance", description = "Maintenance plugin for FearNixx")
public class MCSMaintenance implements IMCSMaintenance {

    @Inject
    private Logger logger;

    @Inject
    private PluginContainer pluginContainer;

    @Inject
    @DefaultConfig(sharedRoot = false)
    private ConfigurationLoader<CommentedConfigurationNode> configLoader;
    private ConfigurationNode config;

    private FNGMaintenanceManager maintManager;

    //private ILocalizer localizer;

    public MCSMaintenance() {
    }

    @Listener
    public void onGamePreInitializationEvent(GamePreInitializationEvent event) {
        logger.info("Setting up...");
        maintManager = new FNGMaintenanceManager(this);
        reloadConfig();
        if (!maintManager.initialize()) {
            logger.error("Failed to initialize FNGMaintenanceManager - Server may fall under lock-down!");
        }

        Sponge.getServiceManager().setProvider(this, IMCSMaintenance.class, this);
        Sponge.getServiceManager().setProvider(this, IMaintenanceManager.class, maintManager);
    }

    @Listener
    public void onGameReloadEvent(GameReloadEvent event) {
        logger.info("Reloading...");
        reloadConfig();
    }

    @Listener
    public void onGameStoppingEvent(GameStoppingEvent event) {
        logger.info("Saving configuration...");
        if (maintManager.isEnabled()) maintManager.save();
        saveConfig();
    }

    //// === Actual functionality === ////

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public ConfigurationNode getConfig() {
        return config;
    }

    @Override
    public void reloadConfig() {
        try {
            config = configLoader.load();
        } catch (IOException e) {
            logger.error("Cannot load configuration...", e);
        }
    }

    @Override
    public void saveConfig() {
        try {
            configLoader.save(config);
        } catch (IOException e) {
            logger.error("Cannot save configuration...", e);
        }
    }

    /*
    public ILocalizer getLocalizer() {
        return localizer;
    }*/
}
