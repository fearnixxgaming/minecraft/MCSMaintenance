package de.fearnixx.sponge.mcsmaintenance.commands;

import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import de.fearnixx.sponge.mcsmaintenance.set.IMaintenanceSet;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Life4YourGames on 23.11.16.
 */
public class MaintListCommand implements CommandExecutor {

    public static CommandSpec commandSpec(IMCSMaintenance plugin) {
        return CommandSpec.builder()
                .description(Text.of("List maintenance sets"))
                .arguments(GenericArguments.none())
                .executor(new MaintListCommand(plugin))
                .permission(MCSMaintenancePermissions.MAINT_SET_LIST)
                .build();
    }

    private IMCSMaintenance plugin;

    public MaintListCommand(IMCSMaintenance plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Cannot toggle maintenance: IMaintenanceManager not provided!");
            throw new CommandException(Text.of("Maintenance is disabled due to an error!"));
        }
        if (!maintManager.get().isEnabled()) throw new CommandException(Text.of("Maintenance is disabled!"));
        ConfigurationNode sets = maintManager.get().getConfig().getNode("sets");

        Map<String, IMaintenanceSet> setsAvailable = maintManager.get().getAvailableSets();
        List<Text> texts = new ArrayList<Text>();
        List<IMaintenanceSet> activeSets = maintManager.get().getActiveSets();

        setsAvailable.forEach((k, v) -> {
            boolean enabled = activeSets.stream().anyMatch(s -> s.getName().equals(k));
            Text t = Text.of(
                    TextActions.runCommand(v.getInfoCommand()),
                    TextActions.showText(Text.of(TextColors.GRAY, "Click to show info about: ", TextColors.YELLOW, k)),
                    TextColors.GRAY, "* ", (enabled ? TextColors.GREEN : TextColors.YELLOW), k, "\n");
            texts.add(t);
        });
        src.sendMessage(Text.of(
                MaintCommand.CHATDIV,
                Text.join(texts.toArray(new Text[texts.size()])),
                MaintCommand.CHATDIV
        ));

        return CommandResult.success();
    }
}
