package de.fearnixx.sponge.mcsmaintenance.commands;

import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import de.fearnixx.sponge.mcsmaintenance.set.IMaintenanceSet;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.List;
import java.util.Optional;

/**
 * Created by Life4YourGames on 23.11.16.
 */
public class MaintToggleCommand implements CommandExecutor {

    public static CommandSpec commandSpec(IMCSMaintenance plugin) {
        return CommandSpec.builder()
                .description(Text.of("Enable or disable a set"))
                .arguments(
                        GenericArguments.string(
                                Text.of("name")
                        )
                )
                .executor(new MaintToggleCommand(plugin))
                .permission(MCSMaintenancePermissions.MAINT_SET_TOGGLE)
                .build();
    }

    private IMCSMaintenance plugin;

    public MaintToggleCommand(IMCSMaintenance plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Cannot toggle maintenance: IMaintenanceManager not provided!");
            throw new CommandException(Text.of("Maintenance is disabled due to an error!"));
        }
        if (!maintManager.get().isEnabled()) throw new CommandException(Text.of("Maintenance is disabled!"));
        ConfigurationNode sets = maintManager.get().getConfig().getNode("sets");

        Optional<String> optName = ctx.<String>getOne("name");
        if (!optName.isPresent()) throw new CommandException(Text.of("Set name required"));

        if (!src.hasPermission(MCSMaintenancePermissions.MAINT_SET_TOGGLE + "." + optName.get()))
            throw new CommandException(Text.of("You're not allowed to toggle set: " + optName.get()));

        ConfigurationNode setNode = sets.getNode(optName.get());
        if (setNode.isVirtual()) throw new CommandException(Text.of("Set doesn't exist"));

        List<IMaintenanceSet> l = maintManager.get().getActiveSets();
        boolean[] r = new boolean[]{false};
        l.forEach(s -> {
            if (s.getName().equals(optName.get())) {
                r[0] = true;
                return;
            }
        });
        if (r[0]) {
            maintManager.get().disableSet(optName.get());
            src.sendMessage(Text.of(TextColors.WHITE, "Set \"", TextColors.YELLOW, optName.get(), TextColors.WHITE, "\" ", TextColors.RED, "disabled"));
        } else {
            maintManager.get().enableSet(optName.get());
            src.sendMessage(Text.of(TextColors.WHITE, "Set \"", TextColors.YELLOW, optName.get(), TextColors.WHITE, "\" ", TextColors.GREEN, "enabled"));
        }
        return CommandResult.success();
    }
}
