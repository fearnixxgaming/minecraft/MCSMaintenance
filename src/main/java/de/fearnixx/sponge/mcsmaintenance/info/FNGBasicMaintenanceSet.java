package de.fearnixx.sponge.mcsmaintenance.info;

import com.google.common.reflect.TypeToken;
import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import de.fearnixx.sponge.mcsmaintenance.set.IMaintenanceSet;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Life4YourGames on 26.11.16.
 */
public class FNGBasicMaintenanceSet implements IMaintenanceSet {
    public static final String DEFPERM = null;

    private String name;
    private boolean enforceOnEnable;
    private boolean allowsJoin;
    private List<String> blackListedWorlds;
    private Text motd;
    private String togglePerm;
    private int priority;
    private ConfigurationNode lastNode;
    private IMCSMaintenance plugin;

    public FNGBasicMaintenanceSet(ConfigurationNode node, IMCSMaintenance plugin) {
        this.plugin = plugin;
        reload(node);
    }

    @Override
    public void reload() {
        reload(null);
    }

    public void reload(/*@Nullable*/ ConfigurationNode node) {
        if (node==null) node = lastNode;
        else lastNode = node;

        if (node.getNode("name").isVirtual())
            name = node.getKey().toString();
        else
            name = node.getNode("name").getString(null);

        enforceOnEnable = node.getNode("enforceonenable").getBoolean(true);
        allowsJoin = node.getNode("allowjoin").getBoolean(true);
        blackListedWorlds = new ArrayList<String>();
        try {
            node.getNode("deniedworlds").getList(TypeToken.of(String.class)).forEach(blackListedWorlds::add);
        } catch (ObjectMappingException e) {
            //
        }
        if (node.getNode("motd").isVirtual())
            motd = null;
        else
            motd = TextSerializers.FORMATTING_CODE.deserialize(node.getNode("motd").getString("&eNULL"));
        togglePerm = node.getNode("perm").getString(DEFPERM);
        priority = node.getNode("priority").getInt(0);
    }

    public ConfigurationNode getNode() {
        return lastNode;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean enforceOnEnable() {
        return enforceOnEnable;
    }

    @Override
    public boolean restrictsJoin() {
        return !allowsJoin;
    }

    @Override
    public boolean checkPlayerAgainstServer(User user) {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent() && !allowsJoin) return false;
        return allowsJoin || user.hasPermission(MCSMaintenancePermissions.MAINT_EXEMPT_JOIN) ||
        user.hasPermission(MCSMaintenancePermissions.MAINT_EXEMPT)|| maintManager.get().playerRequestExemption(user);
    }

    @Override
    public Optional<Transform<World>> checkPlayerAgainstWorld(User user, Transform<World> to, Transform<World> from) {
        if (from!=null) return fromProvided(user, to, from);
        else return fromMissing(user, to);
    }

    private Optional<Transform<World>> fromProvided(User user, Transform<World> to, Transform<World> from) {
        boolean interWorldTeleport = !to.getExtent().getName().equals(from.getExtent().getName());
        if (!interWorldTeleport) return Optional.empty();

        boolean fromDeniedWorld = blackListedWorlds.contains(from.getExtent().getName());
        boolean toDeniedWorld = blackListedWorlds.contains(to.getExtent().getName());
        if (!toDeniedWorld) return Optional.empty();

        if (!fromDeniedWorld) {
            //Just deny the teleport
            /*if (user instanceof Player)
                ((Player) user).sendMessage(Text.of(TextColors.RED, "Teleport denied: The world you were trying to enter is in maintenance."));
            */return Optional.of(from);
        } else {
            String fallBackWorldName = Sponge.getServer().getDefaultWorldName();
            World world = Sponge.getServer().getWorld(fallBackWorldName).orElse(null);
            if (world == null)
                world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorld().get().getUniqueId()).get();
            Location<World> loc = world.getSpawnLocation();
            Transform<World> redir = new Transform<World>(loc.getExtent(), loc.getPosition());

            to.setExtent(redir.getExtent());
            to.setLocation(redir.getLocation());
            if (user instanceof Player) {
                //((Player) user).sendMessage(Text.of(TextColors.RED, "The world you were trying to enter is currently in maintenance, you've been redirected."));
            }
            return Optional.of(redir);
        }
    }

    private Optional<Transform<World>> fromMissing(User user, Transform<World> to) {
        //boolean fromDeniedWorld = deniedWorlds.contains(from.getExtent().getName());
        boolean toDeniedWorld = blackListedWorlds.contains(to.getExtent().getName());
        if (!toDeniedWorld) return Optional.empty();

        boolean hasExemptPermission = (
                user.hasPermission(MCSMaintenancePermissions.MAINT_EXEMPT)
                || user.hasPermission(MCSMaintenancePermissions.MAINT_EXEMPT_WORLDS)
                || (user.hasPermission(MCSMaintenancePermissions.MAINT_EXEMPT_WORLD + '.' + to.getExtent().getName()))
        );

        if (hasExemptPermission) {
            if (user instanceof Player) {
                //((Player) user).sendMessage(Text.of(TextColors.YELLOW, "Notice: This world is currently in maintenance."));
            }
            return Optional.empty();
        }

        String fallBackWorldName = Sponge.getServer().getDefaultWorldName();
        World world = Sponge.getServer().getWorld(fallBackWorldName).orElse(null);
        if (world == null)
            world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorld().get().getUniqueId()).get();
        Location<World> loc = world.getSpawnLocation();
        Transform<World> redir = new Transform<World>(loc.getExtent(), loc.getPosition());

        if (user instanceof Player) {
            //((Player) user).sendMessage(Text.of(TextColors.RED, "The world you were trying to enter is currently in maintenance, you've been redirected."));
        }
        return Optional.of(redir);
    }

    @Override
    public Optional<Text> getMOTD() {
        return Optional.ofNullable(motd);
    }

    @Override
    public Optional<String> getTogglePerm() {
        return Optional.ofNullable(togglePerm);
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public Text getInfo() {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        boolean enabled = maintManager.isPresent() && maintManager.get().getActiveSets().contains(this);
        List<Text> info = new ArrayList<Text>();
        info.add(Text.of(TextActions.showText(Text.of(TextColors.GRAY, TextStyles.ITALIC, "Reload info")),
                TextActions.runCommand("/maint info " + getName()),
                TextColors.WHITE, "Name: ", TextColors.GRAY, getName()));
        info.add(Text.of(
                TextActions.showText(Text.of(TextColors.GRAY, "Click to toggle ", TextColors.GOLD, name)),
                TextActions.runCommand("/maint toggle " + name),
                TextColors.WHITE, "Status: ", enabled ? (Text.of(TextColors.GREEN, "Enabled")) : (Text.of(TextColors.YELLOW, "Disabled - Available"))));
        info.add(Text.of(
                TextActions.showText(Text.of(TextColors.GRAY, "Click to change priority")),
                TextActions.suggestCommand("/maint edit " + name + " priority "),
                TextColors.WHITE, "Priority: ", TextColors.GRAY, getPriority()));
        info.add(Text.of(
                TextActions.showText(Text.of(TextColors.GRAY, "Click to edit rules")),
                TextActions.suggestCommand("/maint edit " + getName() + ' '),
                TextColors.WHITE, "Rules: "));
        if (enforceOnEnable)
            info.add(Text.of(TextColors.WHITE, " - ", TextColors.RED, "Enforce on current players"));
        if (!allowsJoin)
            info.add(Text.of(TextColors.WHITE, " - ", TextColors.RED, "Reject connection events"));
        if (motd!=null)
            info.add(Text.of(
                    TextActions.showText(Text.of(TextColors.GRAY, "Click to change the motd")),
                    TextActions.suggestCommand("/maint edit " + name + " motd "),
                    TextColors.WHITE, " - Change MOTD to: ", motd));
        if (blackListedWorlds.size()>0) {
            info.add(Text.of(TextColors.WHITE, " - Restrict entry to these worlds:"));
            final List<Text> lambdaList = info;
            blackListedWorlds.forEach(w -> {
                lambdaList.add(Text.of(
                        TextActions.showText(Text.of(TextColors.GRAY, "Click to remove ", TextColors.RED, w, TextColors.WHITE, " from the blacklist")),
                        TextActions.suggestCommand("/maint edit " + name + " deniedworlds -" + w),
                        TextColors.WHITE, " --> ", TextColors.RED, w));
            });
        }
        if (togglePerm!=null)
            info.add(Text.of(TextColors.WHITE, " - May only be toggled by users granted: ", TextColors.GRAY ,togglePerm));

        info.add(Text.of("\n"));
        return Text.joinWith(Text.of("\n"), info.toArray(new Text[info.size()]));
    }

    @Override
    public String getEditCommand() {
        return "/maint edit " + getName();
    }

    @Override
    public String getInfoCommand() {
        return "/maint info " + getName();
    }
}
